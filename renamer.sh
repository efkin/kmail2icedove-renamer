#!/bin/bash

for file in $(find mail/ -type f); do
    filename="${file##*/}"
    base="${filename%.*}"
    path=$(dirname "${file}")
    mv "$file" "$path/$base.eml"
done
